﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using BusinessLayer.Model;

namespace DataAccessLayer
{
    public partial class EmployeeManagementJSON
    {
        private ValidationComponent objValidate = null;
        string FileFullPath = "";
        public EmployeeManagementJSON()
        {
            objValidate = new ValidationComponent();
            string FileName = "EmployeeData.json";
            string FilePath =  HttpContext.Current.Request.PhysicalApplicationPath;
            FileFullPath = Path.Combine(FilePath, "bin", FileName);
        }


        //Reading JSON File
        public List<Employee> ReadEmployeeDataJson()
        {
            if (!File.Exists(FileFullPath))
                return null;
            else
            {
                var jsonEmployeeData = File.ReadAllText(FileFullPath);
                return JsonConvert.DeserializeObject<List<Employee>>(jsonEmployeeData);
            }
        }

        //Writing to JSON File
        public bool WriteEmployeeDataJson(List<Employee> EmployeeData)
        {
            if (EmployeeData != null && EmployeeData.Count > 0)
            {
                var jsonEmployeeData = JsonConvert.SerializeObject(EmployeeData, Formatting.Indented);
                File.WriteAllText(FileFullPath, jsonEmployeeData);
                return true;
            }
            else
                return false;
        }
    }
}