﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Model;

namespace DataAccessLayer.Interfaces
{
    public interface IEmployeeManager
    {
        List<Employee> ViewEmployee();
        List<Employee> ViewEmployee(string employeeLastName,string employeePhone);
        bool CreateEmployee(Employee objEmployee);
        bool UpdateEmployee(Employee objEmployee);
        bool DeleteEmployee(int id);
        Employee GetEmployee(int Id);
    }
}
