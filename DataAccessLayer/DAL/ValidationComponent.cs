﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Model;

namespace DataAccessLayer
{
    public class ValidationComponent
    {
        //Validating Data before DB Operations Start
        public bool ValidateEmployeeData(Employee objEmployee)
        {
            if (string.IsNullOrEmpty(objEmployee.EmployeeFirstName))
                return false;
            else if (string.IsNullOrEmpty(objEmployee.EmployeeLastName))
                return false;
            else if (string.IsNullOrEmpty(objEmployee.EmployeePhone))
                return false;
            else if (string.IsNullOrEmpty(objEmployee.EmployeeZip))
                return false;
            else if (string.IsNullOrEmpty(objEmployee.HireDate))
                return false;
            return true;
        }
    }
}