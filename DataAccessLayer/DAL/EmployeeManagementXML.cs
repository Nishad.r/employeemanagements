﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Model;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    public class EmployeeManagementXML : IEmployeeManager
    {
        public EmployeeManagementXML()
        {
        }


        //Get Employees from Storage
        public List<Employee> ViewEmployee()
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Employees from Storage of matching Last Name and Phone
        public List<Employee> ViewEmployee(string employeeLastName, string employeePhone)
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Employee using EmployeeID
        public Employee GetEmployee(int Id)
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Insering Employee details in Storage
        public bool CreateEmployee(Employee objEmployee)
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Editing Employee Details
        public bool UpdateEmployee(Employee objEmployee)
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Deleteing an Employee from Storage
        public bool DeleteEmployee(int id)
        {
            try
            {
                //implementation pending
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}