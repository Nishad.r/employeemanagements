﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Model;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    public partial class EmployeeManagementJSON : IEmployeeManager
    {

        //Get Employees from Storage
        public List<Employee> ViewEmployee()
        {
            try
            {
                var employees = ReadEmployeeDataJson().OrderBy(x => Convert.ToDateTime(x.HireDate)).ToList(); 
                return employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Employees from Storage of matching Last Name and Phone
        public List<Employee> ViewEmployee(string employeeLastName, string employeePhone)
        {
            try
            {
                var employees = ReadEmployeeDataJson();
                var employeeFilterList = (from p in employees where p.EmployeeLastName == ((employeeLastName == "") ? p.EmployeeLastName : employeeLastName) && p.EmployeePhone == ((employeePhone == "") ? p.EmployeePhone : employeePhone) select p).ToList().OrderBy(x=> Convert.ToDateTime( x.HireDate)).ToList();
                return employeeFilterList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get Employee using EmployeeID
        public Employee GetEmployee(int employeeID)
        {
            try
            {
                var result = ReadEmployeeDataJson().FirstOrDefault(b => b.EmployeeID == employeeID);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Insering Employee details in Storage
        public bool CreateEmployee(Employee objEmployee)
        {
            try
            {
                //Validate Employee Details
                if (!objValidate.ValidateEmployeeData(objEmployee) || objEmployee == null)
                    return false;
                int employeeID = 0;

                //Read data from JSON File
                var employees = ReadEmployeeDataJson();
                if (employees != null && employees.Count > 0)
                    employeeID = employees.Max(x => x.EmployeeID);
                else
                    employees = new List<Employee>();
                objEmployee.EmployeeID = employeeID + 1;
                employees.Add(objEmployee);

                //Write to JSON File
                return WriteEmployeeDataJson(employees);
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        //Editing Employee Details
        public bool UpdateEmployee(Employee objEmployee)
        {
            try
            {
                if (!objValidate.ValidateEmployeeData(objEmployee) || objEmployee == null)
                    return false;
                var Employees = ReadEmployeeDataJson();
                if (Employees != null && Employees.Count > 0)
                {
                    Employees.ForEach(x =>
                    {
                        if (x.EmployeeID == objEmployee.EmployeeID)
                        {
                            x.EmployeeFirstName = objEmployee.EmployeeFirstName;
                            x.EmployeeLastName = objEmployee.EmployeeLastName;
                            x.EmployeePhone = objEmployee.EmployeePhone;
                            x.EmployeeZip = objEmployee.EmployeeZip;
                            x.HireDate = objEmployee.HireDate;
                        }
                    }
                    );

                    return WriteEmployeeDataJson(Employees);
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }


        //Deleteing an Employee from Storage
        public bool DeleteEmployee(int EmployeeID)
        {
            try
            {
                var employees = ReadEmployeeDataJson();
                if (employees != null && employees.Count() > 0)
                {
                    var employee = employees.FirstOrDefault(x => x.EmployeeID == EmployeeID);
                    if (employee == null)
                        return false;
                    employees.Remove(employee);
                    return WriteEmployeeDataJson(employees);
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }
    }
}