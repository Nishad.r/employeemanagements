﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.Model;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    public class EmployeeManagementDB : IEmployeeManager
    {
        private EmployeeDbContext db = null;
        private ValidationComponent objValidate = null;

        public EmployeeManagementDB()
        {
            db = new EmployeeDbContext();
            objValidate = new ValidationComponent();
        }

        //Get Employees from Storage
        public List<Employee> ViewEmployee()
        {
            try
            {
                var employees = db.Employees.ToList().OrderBy(x => Convert.ToDateTime(x.HireDate)).ToList(); 
                return employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Employees from Storage of matching Last Name and Phone
        public List<Employee> ViewEmployee(string employeeLastName, string employeePhone)
        {
            try
            {
                var employees = db.Employees.ToList().OrderBy(x => Convert.ToDateTime(x.HireDate)).ToList(); ;
                var employeeFilterList = (from p in employees where p.EmployeeLastName == ((employeeLastName == "") ? p.EmployeeLastName : employeeLastName) && p.EmployeePhone == ((employeePhone == "") ? p.EmployeePhone : employeePhone) select p).ToList().OrderBy(x => Convert.ToDateTime(x.HireDate)).ToList();
                return employeeFilterList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Get Employee using EmployeeID
        public Employee GetEmployee(int employeeID)
        {
            try
            {
                var result = db.Employees.SingleOrDefault(b => b.EmployeeID == employeeID);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Insering Employee details in Storage
        public bool CreateEmployee(Employee objEmployee)
        {
            try
            {
                if (!objValidate.ValidateEmployeeData(objEmployee) || objEmployee == null)
                    return false;
                var Employee = db.Set<Employee>();
                Employee.Add(objEmployee);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }


        //Editing Employee Details
        public bool UpdateEmployee(Employee objEmployee)
        {
            try
            {
                if (!objValidate.ValidateEmployeeData(objEmployee) || objEmployee == null)
                    return false;
                var result = db.Employees?.ToList()?.SingleOrDefault(b => objEmployee != null && b.EmployeeID == objEmployee.EmployeeID);
                if (result != null)
                {
                    result.EmployeeFirstName = objEmployee.EmployeeFirstName;
                    result.EmployeeLastName = objEmployee.EmployeeLastName;
                    result.EmployeePhone = objEmployee.EmployeePhone;
                    result.EmployeeZip = objEmployee.EmployeeZip;
                    result.HireDate = objEmployee.HireDate;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }


        //Deleteing an Employee from Storage
        public bool DeleteEmployee(int EmployeeID)
        {
            try
            {
                var itemToRemove = db.Employees.SingleOrDefault(x => x.EmployeeID == EmployeeID);
                if (itemToRemove != null)
                {
                    db.Employees.Remove(itemToRemove);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }
    }
}