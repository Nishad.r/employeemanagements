﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmployeeManagement._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Employee Management System</h1>
        <p class="lead">This Employee management System software, helps your employees to give their best efforts every day to achieve the goals of your organization..</p>
        <p><a href="Employee/AddEmployee" class="btn btn-primary btn-lg">Add New Employee &raquo;</a></p>
    </div>

    

</asp:Content>
