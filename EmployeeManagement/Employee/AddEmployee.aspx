﻿<%@ Page Title="Add Employee" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="EmployeeManagement.AddEmployee" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <h2>Add Employee</h2>

    <table>
        <tr>
            <td>First Name   </td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            </td>
            <td style="width: 173px">
                <asp:RequiredFieldValidator ID="RqdValidatorFname" runat="server" ControlToValidate="txtFirstName" ErrorMessage="RequiredFieldValidator" ForeColor="Red">First Name is Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Last Name   </td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </td>
            <td style="width: 173px">
                <asp:RequiredFieldValidator ID="RqdValidatorLname" runat="server" ControlToValidate="txtLastName" ErrorMessage="RequiredFieldValidator" ForeColor="Red">Last Name is Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Phone   </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
            <td style="width: 173px">
                <asp:RequiredFieldValidator ID="RqdValidatorPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="RequiredFieldValidator" ForeColor="Red">Phone is Required</asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="RegExPhone" runat="server" ControlToValidate="txtPhone"
                    ErrorMessage="Please enter in valid format , format is (XXX)XXX-XXXX" ForeColor="Red" ValidationExpression="^[(]\d{3}[)][ ]?\d{3}[-]\d{4}$">  
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>Zip Code   </td>
            <td>
                <asp:TextBox ID="txtZip" runat="server" TextMode="Number"></asp:TextBox>
            </td>
            <td style="width: 173px">
                <asp:RequiredFieldValidator ID="RqdValidatorZip" runat="server" ControlToValidate="txtZip" ErrorMessage="RequiredFieldValidator" ForeColor="Red">Zip is Required</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Hire Date   </td>
            <td>
                <asp:TextBox ID="txtHireDate" runat="server" ClientIDMode="Static"></asp:TextBox>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#txtHireDate").datepicker({
                            dateFormat: "mm/dd/yy",
                            changeMonth: true,
                            changeYear: true,
                            yearRange: "-50:+0"
                        }).on('change', function () {
                        });

                    });
                </script>
            </td>
            <td style="width: 173px">
                <asp:RequiredFieldValidator ID="RqdValidatorHireDate" runat="server" ControlToValidate="txtHireDate" ErrorMessage="RequiredFieldValidator" ForeColor="Red">Hire Date Required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RgXHireDate" runat="server" ControlToValidate="txtHireDate"
                    ErrorMessage="Please enter in valid format , format is (XXX)XXX-XXXX" ForeColor="Red" ValidationExpression="^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$">  
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="ButtonCreate" runat="server" Text="Create" OnClick="ButtonCreate_Click" />
            </td>
            <td style="width: 173px">
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CausesValidation="False" OnClick="ButtonCancel_Click" />
            </td>
        </tr>
    </table>

</asp:Content>
