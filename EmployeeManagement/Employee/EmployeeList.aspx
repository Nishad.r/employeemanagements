﻿<%@ Page Title="Employee List" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeList.aspx.cs" Inherits="EmployeeManagement.EmployeeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Employees List</h2>
    <table>
        <tr>
            <td>Last Name :  </td>
            <td>
                <asp:TextBox ID="txtLastname" runat="server"></asp:TextBox>
            </td>
            <td>Phone:  </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <asp:Button Text="Search" runat="server" OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
    <p>
        <asp:GridView ID="GridViewEmpList" runat="server" BackColor="black" BorderColor="#000099" BorderStyle="Dashed" CssClass="bg-primary"
            EmptyDataText="No employees Found" DataKeyNames="EmployeeID" OnRowDeleting="GridViewEmpList_RowDeleting">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
    </p>
    <script>
        $(document).on("click", "a", function () {
            if (this.innerHTML.indexOf("Delete") == 0) {
                return confirm("Are you sure you want to delete");
            }
        });
    </script>
</asp:Content>
