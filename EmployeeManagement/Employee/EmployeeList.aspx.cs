﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BusinessLayer.Model;
using DataAccessLayer.Interfaces;

namespace EmployeeManagement
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        public IEmployeeManager EmployeeManager;

        //Constructor
        public EmployeeList()
        {
            //Switch between DB Storages(JSON / XML /SQL DB)
            EmployeeManager = new EmployeeManagementJSON();     //Using JSON File as DB Storage
            //EmployeeManager = new EmployeeManagementDB();     //Correct SQL DB  Details in WebConfig and Enable this to use  SQL as DB Storage
            //EmployeeManager = new EmployeeManagementXML();    //Using XML as DB Storage/ Not Implememnted
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ///Binding in GridView while PageLoad
                GridViewEmpList.DataSource = EmployeeManager.ViewEmployee();
                GridViewEmpList.DataBind();
            }
        }



        //Deleting an Employee
        protected void GridViewEmpList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var empId = Convert.ToInt32(e.Keys["EmployeeID"]);
            bool result = EmployeeManager.DeleteEmployee(empId);
            if (result)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('employee deleted successfully');", true);
                GridViewEmpList.DataSource = EmployeeManager.ViewEmployee();
                GridViewEmpList.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('failled to delete employee');", true);
            }
        }

        //Searching Employees using Last Name and Phone
        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            var EmployeeList = EmployeeManager.ViewEmployee(txtLastname.Text.Trim(), txtPhone.Text.Trim());

            if (EmployeeList != null && EmployeeList.Count>0)
            {
                GridViewEmpList.DataSource = EmployeeList;
                GridViewEmpList.DataBind();
            }
            else
            {
                GridViewEmpList.DataSource = null;
                GridViewEmpList.DataBind();
            }
        }

    }
}