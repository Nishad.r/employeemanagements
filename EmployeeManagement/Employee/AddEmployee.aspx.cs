﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer;
using BusinessLayer.Model;
using DataAccessLayer.Interfaces;
namespace EmployeeManagement
{
    public partial class AddEmployee : System.Web.UI.Page
    {
        public IEmployeeManager EmployeeManager;

        //Constructor
        public AddEmployee()
        {
            //Switch between DB Storages(JSON / XML /SQL DB)
            EmployeeManager = new EmployeeManagementJSON();     //Using JSON File as DB Storage
            //EmployeeManager = new EmployeeManagementDB();     //Correct SQL DB  Details in WebConfig and Enable this to use  SQL as DB Storage
            //EmployeeManager = new EmployeeManagementXML();    //Using XML as DB Storage/ Not Implememnted
        }


        //Adding an Employee
        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            Employee employee = new Employee
            {
                EmployeeLastName = txtLastName.Text.Trim(),
                EmployeeFirstName = txtFirstName.Text.Trim(),
                EmployeePhone = txtPhone.Text.Trim(),
                EmployeeZip = txtZip.Text.Trim(),
                HireDate = txtHireDate.Text
            };
            bool result = EmployeeManager.CreateEmployee(employee);
            if (result)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('employee added successfully');window.location='EmployeeList.aspx';", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('failled to add employee');", true);
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}