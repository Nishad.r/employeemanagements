﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BusinessLayer.Model
{
    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext() : base("name = EmployeeDbContext")
        {

        }
        //setting EF convention
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //use singular table name
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Employee> Employees { get; set; }
    }
}